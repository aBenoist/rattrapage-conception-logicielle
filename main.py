import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel

class ajouter(BaseModel):
    nombre: int
    nombre2: int

class soustraire(BaseModel):
    nombre: int
    nombreASoustraire: int

class multiplier(BaseModel):
    nombre: int
    nombre2: int

class diviser(BaseModel):
    numerateur: int
    denominateur: int


calculette = FastAPI()


@calculette.post("/ajouter")
async def create_item(item: ajouter):
    total = item.nombre + item.nombre2
    return {"resultat": total}

@calculette.post("/soustraire")
async def create_item(item: soustraire):
    total = item.nombre - item.nombreASoustraire
    return {"resultat": total}

@calculette.post("/multiplier")
async def create_item(item: multiplier):
    total = item.nombre * item.nombre2
    return {"resultat": total}

@calculette.post("/diviser")
async def create_item(item: diviser):
    total = int(item.numerateur/item.denominateur)
    return {"resultat": total}

if __name__ == "__main__":
    uvicorn.run(calculette, host="0.0.0.0", port=8000)
